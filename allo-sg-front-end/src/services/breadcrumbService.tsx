import { createContext, useState, ReactNode, FC } from 'react';
import { Breadcrumb } from './breadcrumb-interface';

export interface IBreadcrumbService {
    getBreadcrumbs(): Breadcrumb[];
    setBreadcrumbs(breadcrumbs: Breadcrumb[]): void;
}

const defaultBreadcrumbService: IBreadcrumbService = {
    getBreadcrumbs: () => [],
    setBreadcrumbs: () => {}
};

export const BreadcrumbServiceContext = createContext<IBreadcrumbService>(defaultBreadcrumbService);

const BreadcrumbServiceProvider: FC<{ children: ReactNode }> = ({ children }) => {
    const [breadcrumbs, setBreadcrumbs] = useState<Breadcrumb[]>([]);

    const breadcrumbService: IBreadcrumbService = {
        getBreadcrumbs: () => breadcrumbs,
        setBreadcrumbs: (newBreadcrumbs: Breadcrumb[]) => {
            setBreadcrumbs(newBreadcrumbs);
        }
    };

    return (
        <BreadcrumbServiceContext.Provider value={breadcrumbService}>
            {children}
        </BreadcrumbServiceContext.Provider>
    );
};

export default BreadcrumbServiceProvider;

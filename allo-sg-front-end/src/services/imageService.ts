// src/services/imageService.ts
import axios, { AxiosResponse } from 'axios';

const apiClient = axios.create({
  baseURL: 'http://localhost:1337/api',
  headers: {
    Authorization: `Bearer ${process.env.REACT_APP_STRAPI_AUTH_TOKEN}`,
  },
});

export const uploadImage = async (image: File): Promise<string> => {
  try {
    const formData = new FormData();
    formData.append('files', image);

    const response: AxiosResponse = await apiClient.post('/upload', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });

    // Retourne l'ID de l'image uploadée
    return response.data[0].id;
  } catch (error) {
    // Gérer les erreurs d'upload ici
    console.error('Une erreur est survenue lors de l\'upload de l\'image :', error);
    throw new Error('Une erreur est survenue lors de l\'upload de l\'image');
  }
};

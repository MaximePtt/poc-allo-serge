// src/services/apiService.ts
import axios from 'axios';
import { ApiResponse } from '../model/ApiResponse';

const apiClient = axios.create({
  baseURL: process.env.REACT_APP_API_BASE_URL,
  headers: {
    Authorization: `Bearer ${process.env.REACT_APP_STRAPI_AUTH_TOKEN}`,
  },
});

export const fetchArticles = async (pageSize: number, pageNb: number): Promise<ApiResponse> => {
  let queryUrl = '/articles'
  queryUrl += `?pagination[pageSize]=${pageSize}`
  queryUrl += `&pagination[page]=${pageNb}`
  queryUrl += `&pagination[withCount]=true`
  queryUrl += `&populate[0]=thumbnail`
  const response = await apiClient.get<ApiResponse>(queryUrl);
  return response.data;
};

export const fetchArticle = async (articleId: number): Promise<ApiResponse> => {
  let queryUrl = `/articles/${articleId}`
  queryUrl += `?populate[0]=thumbnail`
  const response = await apiClient.get<ApiResponse>(queryUrl);
  return response.data;
};

export const createArticle = async (title: string, content: string, thumbnailUrl: string): Promise<void> => {
  try {
    const response = await apiClient.post('/articles', {
      data: {
        Title: title,
        Content: content,
        thumbnail: thumbnailUrl,
      },
    });

    // Vérifiez le statut de la réponse si nécessaire
    if (response.status === 201 || response.status === 200) {
      console.log('Article créé avec succès !');
    } else {
      console.error('Erreur lors de la création de l\'article :', response.data);
      throw new Error('Une erreur est survenue lors de la création de l\'article');
    }
  } catch (error) {
    console.error('Une erreur est survenue lors de la création de l\'article :', error);
    throw new Error('Une erreur est survenue lors de la création de l\'article');
  }
};
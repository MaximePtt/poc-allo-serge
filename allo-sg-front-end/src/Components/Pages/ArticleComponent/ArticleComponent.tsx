// src/components/Articles.tsx
import React, { useContext, useEffect, useState } from 'react';
import { fetchArticle } from '../../../services/articlesService';
import { Article } from '../../../model/Article';
import './ArticleComponent.scss'
import { useParams } from 'react-router-dom';
import { BreadcrumbServiceContext } from '../../../services/breadcrumbService';
import { Breadcrumb } from '../../../services/breadcrumb-interface';

const ArticleComponent: React.FC = () => {
  const breadcrumbService = useContext(BreadcrumbServiceContext);
  
  const [article, setArticle] = useState<Article | null>(null);
  const [error, setError] = useState<string | null>(null);
  const [articleLoaded, setArticleLoaded] = useState<boolean>(false);
  const { articleId } = useParams();

  useEffect(() => {
    const getArticle = async () => {
      try {
        const data = await fetchArticle(Number(articleId));
        setArticle(data.data);
      } catch (error) {
        setError('Une erreur est survenue lors de la récupération de l\'article.');
      } finally {
        setArticleLoaded(true);
      }
    };

    getArticle();
  }, []);

  
  useEffect(() => {
    const breadcrumbs: Breadcrumb[] = [
      { path: '/', label: 'Accueil' },
      { path: '/articles', label: 'Liste des articles' },
      { path: `/article/${articleId}`, label: article ? article.attributes.Title : "Article inconnu" }
    ];
    
    breadcrumbService.setBreadcrumbs(breadcrumbs);
  }, [breadcrumbService, articleLoaded]);

  if (!articleLoaded) return <p>Chargement...</p>;
  if (error) return <p>{error}</p>;

  return (
    article &&
      <div className='article-container'>
        <div className="img" style={{ backgroundImage: `url(${process.env.REACT_APP_IMG_BASE_URL}${article.attributes.thumbnail.data?.attributes.url})` }}></div>
        <h2 className="title">{article.attributes.Title}</h2>
        <p className="description">{article.attributes.Content}</p>
        <p className="creation-date">Créé le: {new Date(article.attributes.createdAt).toLocaleDateString()}</p>
      </div>
  );
};

export default ArticleComponent;

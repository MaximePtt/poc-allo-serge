// src/components/Articles.tsx
import React, { useContext, useEffect, useState } from 'react';
import './CreateArticleComponent.scss';
import { Breadcrumb } from '../../../services/breadcrumb-interface';
import { BreadcrumbServiceContext } from '../../../services/breadcrumbService';
import { uploadImage } from '../../../services/imageService';
import { createArticle } from '../../../services/articlesService';

const CreateArticleComponent: React.FC = () => {
  const breadcrumbService = useContext(BreadcrumbServiceContext);

  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [error, setError] = useState<string | null>(null);
  const [info, setInfo] = useState<string | null>(null);
  const [title, setTitle] = useState<string>('');
  const [content, setContent] = useState<string>('');

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      setSelectedFile(file);
    }
  };

  const handleSubmit = async (event: React.FormEvent) => {
    event.preventDefault();

    if (!title.trim() || !content.trim()) {
      setError('Veuillez remplir tous les champs.');
      return;
    }

    try {
      if (!selectedFile) {
        setError('Veuillez sélectionner un fichier.');
        return;
      }

      const imageId = await uploadImage(selectedFile);

      // Une fois l'image téléchargée, créer l'article avec le lien de l'image
      await createArticle(title, content, imageId);
      setError(null);
      setInfo('Article créé avec succès !')
    } catch (error) {
      setError('Une erreur est survenue lors de la création de l\'article.');
    }
  };

  useEffect(() => {
    const breadcrumbs: Breadcrumb[] = [
      { path: '/', label: 'Accueil' },
      { path: '/articles', label: 'Articles' },
      { path: '/create-article', label: 'Soumettre un article' },
    ];

    breadcrumbService.setBreadcrumbs(breadcrumbs);
  }, [breadcrumbService]);

  return (
    <div className='create-article-container'>
      <h2 className='title'>Soumettre un article</h2>
      <div className='form-container'>
        <form onSubmit={handleSubmit}>
          <div className='form-field'>
            <label htmlFor="thumbnail">Miniature :</label>
            <input className='input' type="file" id="thumbnail" onChange={handleFileChange} />
          </div>
          <div className='form-field'>
            <label htmlFor="title">Titre :</label>
            <input className='input' type="text" id="title" placeholder="Titre de l'article" value={title} onChange={(e) => setTitle(e.target.value)} />
          </div>
          <div className='form-field'>
            <label htmlFor="content">Contenu :</label>
            <textarea className='input' id="content" placeholder="Contenu de l'article" value={content} onChange={(e) => setContent(e.target.value)} />
          </div>
          <button className='primary-button' type="submit">Créer l'article</button>
          {error && <p>{error}</p>}
          {info && <p>{info}</p>}
        </form>
      </div>
    </div>
  );
};

export default CreateArticleComponent;

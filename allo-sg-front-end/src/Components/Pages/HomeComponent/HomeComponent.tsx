// src/components/Articles.tsx
import React, { useContext, useEffect } from 'react';
import './HomeComponent.scss'
import { useNavigate } from 'react-router-dom';
import { Breadcrumb } from '../../../services/breadcrumb-interface';
import { BreadcrumbServiceContext } from '../../../services/breadcrumbService';

const HomeComponent: React.FC = () => {
  const breadcrumbService = useContext(BreadcrumbServiceContext);
  
  const navigate = useNavigate()

  useEffect(() => {
    const breadcrumbs: Breadcrumb[] = [
        { path: '/', label: 'Accueil' }
    ];
    
    breadcrumbService.setBreadcrumbs(breadcrumbs);
  }, [breadcrumbService]);

  function onClickConsultArticlesButton(){
    navigate("/articles")
  }

  return (
    <div className='home-container'>
      <h2 className='title'>Bienvenue sur la page d'accueil</h2>
      <p>A développer...</p>
      <button className='primary-button' onClick={() => onClickConsultArticlesButton()}>Consulter les actualités</button>
    </div>
  );
};

export default HomeComponent;

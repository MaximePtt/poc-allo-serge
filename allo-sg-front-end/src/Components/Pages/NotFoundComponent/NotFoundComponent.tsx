// src/components/Articles.tsx
import React, { useContext, useEffect } from 'react';
import './NotFoundComponent.scss'
import { useLocation, useNavigate } from 'react-router-dom';
import { Breadcrumb } from '../../../services/breadcrumb-interface';
import { BreadcrumbServiceContext } from '../../../services/breadcrumbService';

const NotFoundComponent: React.FC = () => {
  const breadcrumbService = useContext(BreadcrumbServiceContext);
  
  const navigate = useNavigate()
  const location = useLocation();

  useEffect(() => {
    const breadcrumbs: Breadcrumb[] = [
        { path: location.pathname, label: 'Page introuvable' }
    ];
    
    breadcrumbService.setBreadcrumbs(breadcrumbs);
  }, [breadcrumbService]);

  function onClickHomeButton(){
    navigate("/")
  }

  return (
    <div className='not-found-container'>
      <div className='title'>Erreur 404</div>
      <div className='text'>La page demandée est introuvable</div>
      <button className='primary-button' onClick={() => onClickHomeButton()}>Retourner à l'accueil</button>
    </div>
  );
};

export default NotFoundComponent;

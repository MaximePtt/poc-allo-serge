import React, { useContext, useEffect, useState } from 'react';
import { fetchArticles } from '../../../services/articlesService';
import { Article } from '../../../model/Article';
import './ArticlesComponent.scss'
import { useNavigate } from 'react-router-dom';
import { Breadcrumb } from '../../../services/breadcrumb-interface';
import { BreadcrumbServiceContext } from '../../../services/breadcrumbService';

const ArticlesComponent: React.FC = () => {
  const breadcrumbService = useContext(BreadcrumbServiceContext);

  const [articles, setArticles] = useState<Article[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [totalPages, setTotalPages] = useState<number>(1);
  const pageSize = 8;
  
  const navigate = useNavigate();

  const getArticles = async (page: number) => {
    setLoading(true);
    try {
      const data = await fetchArticles(pageSize, page);
      setArticles(data.data);
      setTotalPages(Math.ceil(data.meta!.pagination.total / pageSize));
      setCurrentPage(page);
    } catch (error) {
      setError('Une erreur est survenue lors de la récupération des articles.');
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getArticles(1);
  }, []);

  useEffect(() => {
    const breadcrumbs: Breadcrumb[] = [
      { path: '/', label: 'Accueil' },
      { path: '/articles', label: 'Liste des articles' }
    ];
    
    breadcrumbService.setBreadcrumbs(breadcrumbs);
  }, [breadcrumbService]);

  const getPaginationNumbers = () => {
    const pages = [];
    const startPage = Math.max(currentPage - 2, 1);
    const endPage = Math.min(currentPage + 2, totalPages);

    if (startPage > 3) {
      pages.push(1, 2, 3, '...');
    } else {
      for (let i = 1; i < startPage; i++) {
        pages.push(i);
      }
    }

    for (let i = startPage; i <= endPage; i++) {
      pages.push(i);
    }

    if (endPage < totalPages - 2) {
      pages.push('...', totalPages - 2, totalPages - 1, totalPages);
    } else {
      for (let i = endPage + 1; i <= totalPages; i++) {
        pages.push(i);
      }
    }

    return pages;
  };

  const onClickConsultArticleButton = (articleId: number) => {
    navigate("/article/" + articleId);
  };

  const onClickCreateArticleButton = () => {
    navigate("/create-article");
  };

  if (loading) return <p>Chargement...</p>;
  if (error) return <p>{error}</p>;

  return (
    <div className='articles-container'>
      <h1 className='title'>Liste des Articles</h1>
      <div className='articles-list'>
        {articles.map(article => (
          <div key={article.id} className='article'>
            <div className="img-container">
              <div className="img" style={{ backgroundImage: `url(http://localhost:1337${article.attributes.thumbnail!.data?.attributes.url})` }}></div>
            </div>
            <div className="content">
              <h2 className="title">{article.attributes.Title}</h2>
              <p className="description">{article.attributes.Content}</p>
              <p className="creation-date">Créé le: {new Date(article.attributes.createdAt).toLocaleDateString()}</p>
            </div>
            <div className="buttons-container">
              <button className='primary-button' onClick={() => onClickConsultArticleButton(article.id)}>Consulter</button>
            </div>
          </div>
        ))}
      </div>

      <div className="pagination">
        <button disabled={currentPage === 1} onClick={() => getArticles(currentPage - 1)}>Précédent</button>
        {getPaginationNumbers().map((page, index) => (
          page === '...' ? (
            <span key={index}>...</span>
          ) : (
            <button key={index} onClick={() => getArticles(page ? Number(page) : 0)} disabled={currentPage === page}>{page}</button>
          )
        ))}
        <button disabled={currentPage === totalPages} onClick={() => getArticles(currentPage + 1)}>Suivant</button>
      </div>

      <button className='primary-button' onClick={onClickCreateArticleButton}>Soumettre un article</button>
    </div>
  );
};

export default ArticlesComponent;

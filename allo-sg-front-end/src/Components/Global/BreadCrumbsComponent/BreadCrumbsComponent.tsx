// BreadCrumbsComponent.tsx

import React, { useState, useEffect, useContext } from 'react';
import './BreadCrumbsComponent.scss';
import { Breadcrumb } from '../../../services/breadcrumb-interface';
import { BreadcrumbServiceContext } from '../../../services/breadcrumbService';
import { useNavigate } from 'react-router-dom';

const BreadCrumbsComponent: React.FC = () => {
  const breadcrumbService = useContext(BreadcrumbServiceContext);
  
  const [breadcrumbs, setBreadcrumbs] = useState<Breadcrumb[]>(breadcrumbService.getBreadcrumbs());

  const navigate = useNavigate()

  useEffect(() => {
    const updateBreadcrumbs = () => {
      setBreadcrumbs(breadcrumbService.getBreadcrumbs());
    };

    updateBreadcrumbs();
  }, [breadcrumbService]);

  function handleClick(path: string) {
    navigate(path)
  };

  return (
    <div className="breadcrumbs-container">
      <ul>
        {breadcrumbs.map((breadcrumb, index) => (
          <>
            <li key={index} onClick={() => handleClick(breadcrumb.path)}>{breadcrumb.label}</li>
            { (breadcrumbs.length - 1 !== index) && <div className="icon"></div> }
          </>
        ))}
      </ul>
    </div>
  );
};

export default BreadCrumbsComponent;

// src/components/Articles.tsx
import { useNavigate } from 'react-router-dom';
import './HeaderComponent.scss'

const HeaderComponent: React.FC = () => {
  const myAccText = "Mon compte"
  const navigate = useNavigate()

  function onClickLogo(){
    navigate("/")
  }

  return (
    <div className="header-container">
        <div className='top'>
            <div className='my-acc-menu-btn'>{myAccText}</div>
        </div>
        <div className='bottom'>
          <div className='logo' onClick={onClickLogo}></div>
          <div className="nav-buttons-container">
            <button className="header-button">Space planning</button>
            <button className="header-button">Alerter un incident</button>
            <button className="header-button">Gérer mes badges d'accès</button>
          </div>
        </div>
    </div>
  );
};

export default HeaderComponent;

import { Route, Routes } from 'react-router-dom';
import './App.css';
import Header from './Components/Global/HeaderComponent/HeaderComponent';
import BreadCrumbsComponent from './Components/Global/BreadCrumbsComponent/BreadCrumbsComponent';
import BreadcrumbServiceProvider from './services/breadcrumbService';
import HomeComponent from './Components/Pages/HomeComponent/HomeComponent';
import ArticlesComponent from './Components/Pages/ArticlesComponent/ArticlesComponent';
import ArticleComponent from './Components/Pages/ArticleComponent/ArticleComponent';
import NotFoundComponent from './Components/Pages/NotFoundComponent/NotFoundComponent';
import CreateArticleComponent from './Components/Pages/CreateArticleComponent/CreateArticleComponent';

function App() {
  return (
    <div className="app-container">
      <BreadcrumbServiceProvider>
        <Header />
        <BreadCrumbsComponent />
        <Routes>
          <Route path="*" element={<NotFoundComponent />} />
          <Route path="/" element={<HomeComponent />} />
          <Route path="articles" element={<ArticlesComponent />} />
          <Route path="article/:articleId" element={<ArticleComponent />} />
          <Route path="create-article" element={<CreateArticleComponent />} />
        </Routes>
      </BreadcrumbServiceProvider>
    </div>
  );
}

export default App;

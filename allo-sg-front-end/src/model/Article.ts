import { Thumbnail } from "./Thumbnail";

export interface Article {
  id: number;
  attributes: {
    Title: string;
    Content: string;
    createdAt: string;
    updatedAt: string;
    publishedAt: string;
    thumbnail: Thumbnail;
  };
}
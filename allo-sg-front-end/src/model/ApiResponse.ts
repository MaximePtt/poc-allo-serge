import { Article } from "./Article";

export interface ApiResponse {
    data: any;
    meta?: {
      pagination: {
        page: number;
        pageSize: number;
        pageCount: number;
        total: number;
      };
    };
  }